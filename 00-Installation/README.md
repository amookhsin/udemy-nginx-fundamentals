# نصبیدن Nginx

1. بروزیدن بسته‌های سیستم‌عامل

   <div dir="ltr" align="left" markdown="1">

   ```shell
   sudo apt update && sudo apt upgrade && sudo apt full-upgrade
   ```

   </div>

2. نصبیدن nginx

    * ازطریق مدیر بسته

        <div dir="ltr" align="left" markdown="1">

        ```shell
        sudo apt install nginx
        ```

        </div>

    * ازطریق منبع

        <div dir="ltr" align="left" markdown="1">

        ```shell
        # To able to compile the source code
        sudo apt-get install -y build-essential
        # Dependencies
        sudo apt-get install libpcre3 libpcre3-dev zlib1g zlib1g-dev libssl-dev

        # To find latest version: http://nginx.org/en/download.html
        wget http://nginx.org/download/nginx-VERSION.tar.gz -P /tmp/
        tar -zxvf nginx-VERSION.tar.gz

        # Enter to where the nginx file extracted
        cd nginx-VERSION

        # All available options: http://nginx.org/en/docs/configure.html
        ./configure --sbin-path=/usr/sbin/nginx --conf-path=/etc/nginx/nginx.conf --error-log-path=/var/log/nginx/error.log \
            --http-log-path=/var/log/nginx/access.log --pid-path=/run/nginx.pid --with-pcre --with-http_ssl_module
        # To compile this configuration code
        sudo make
        # To install the compiled source
        sudo make install

        # To check nginx available
        nginx -V

        # To start nginx
        sudo nginx

        # To check the process are running
        ps aux | grep nginx
        ```

        </div>

        **رفع مشکل NGINX systemd service file** این فایل باید در مسیر `/lib/systemd/system/nginx.service` ذخیریده شود.

        <div dir="ltr" align="left" markdown="1">

        ```conf
        [Unit]
        Description=The NGINX HTTP and reverse proxy server
        After=syslog.target network-online.target remote-fs.target nss-lookup.target
        Wants=network-online.target

        [Service]
        Type=forking
        PIDFile=/run/nginx.pid
        ExecStartPre=/usr/sbin/nginx -t
        ExecStart=/usr/sbin/nginx
        ExecReload=/usr/sbin/nginx -s reload
        ExecStop=/bin/kill -s QUIT $MAINPID
        PrivateTmp=true

        [Install]
        WantedBy=multi-user.target
        ```

        </div>
