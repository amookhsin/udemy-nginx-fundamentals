# بهینیدن سرور

## نهانیدن (cache) داده‌ها در سمت کاربر

برای نهانیدن داده‌ها در سمت کاربر از سرایندهای (Header) `Cache-Control`، `Pragma` ، `Very` ،و `expires` به‌شکل زیر می‌استفیم.

```conf
# ...
location ~* \.(css|js|png) {
    access_log off;

    # To cache locally
    add_header Cache-Control public;
    add_header Pragma public;
    add_header Very Accept-Encoding;
    expires 60m; # month: 1M, hour: 1h
}
# ...
```

## فشردن پاسخ با `gzip`

فشردن پاسخ کاربر پس از اعلامیدن توانایی گرفتن پاسخ `gzip` :

```conf
#...
http {
  #...

  gzip on;
  gzip_comp_level 4; # 1-9, best 3,4
  gzip_types text/css text/javascript;

  server {
    #..
    location ~* \.(css|js|png) {
      #...
      add_header Very Accept-Encoding;
      #...
    }
  }
}
```

## انباریدن پاسخ منبع‌های زبان پویا (Dynamic Language Resources) در سرور

برای کاهیدن پردازش در سمت سرور از یه ریزنهان (micro cache)، متناسبت با زبان محتوا، استفیده می‌شود. درواقع این ریزنهان محتوای پویا را برای مدتی می‌انبارد تا بتواند به‌شکل محتوای ایستا به مشتری بپاسخد.

![ریزنهان](assets/micro_cache.png)

### پیکریدن ریزنهان (micro cache) برای php

```conf
#...
http {
  #...

  # levels=1:2 -> Ex.: ...194132 -> /tmp/nginx_cache/2/13
  # inactive -> default == 10m 
  fastcgi_cache_path /tmp/nginx_cache levels=1:2 keys_zone=ZONE_1:100m inactive=60m;
  # https://GETdomain.com/blog/article -> $scheme = https://, $request_method = GET, 
  # $request_uri = domain.com, $request_uri = /bog/article
  fastcgi_cache_key "$scheme$request_method$request_uri";

  server {
    #..
    location ~\.php$ {
      #...

      # Enable cache
      fastcgi_cache ZONE_1;
      # to define how long the cache should be valid for each response code
      fastcgi_cache_valid 200 404 60m;
      #fastcgi_cache_valid 200 60m;
      #fastcgi_cache_valid 404 10m;

    }
  }
}
```

**بررسی بهینگی.** ارسال صد درخواست با ده اتصال.

```shell
# Install apache-bench
sudo apt update && sudo apt install apache2-utils
# to check apache-bench installed
ab

# to request 100 to php page with 10 connection at the time
ab -n 100 -c 10 https://167.99.93.26/
```

#### اعلانیدن وضعیت نهانندگی به کاربر

```conf
#...
http {
  #...

  fastcgi_cache_path /tmp/nginx_cache levels=1:2 keys_zone=ZONE_1:100m inactive=60m;
  fastcgi_cache_key "$scheme$request_method$request_uri";
  add_header X-Cache $upstream_cache_state;

  server {
    #..
    location ~\.php$ {
      #...

      fastcgi_cache ZONE_1;
      fastcgi_cache_valid 200 404 60m;
    }
  }
}
```

#### استثناییدن نهانندگی

```conf
#...
http {
  #...

  fastcgi_cache_path /tmp/nginx_cache levels=1:2 keys_zone=ZONE_1:100m inactive=60m;
  fastcgi_cache_key "$scheme$request_method$request_uri";
  add_header X-Cache $upstream_cache_state;

  # Cache by default
  set $no_cache 0;

  # Check for cache bypass
  if ($arg_skipcache = 1) {
    set $no_cache 1;
  }

  server {
    #...
    location ~\.php$ {
      #...
      
      fastcgi_cache ZONE_1;
      fastcgi_cache_valid 200 404 60m;

      fastcgi_cache_bypass $no_cache;
      fastcgi_no_cache $no_cache;
    }
  }
}
```

## استفیدن از HTTP2

برای فعالیدن پروتکل http2 نخست باید پودمانش نصبیده شود.

1. نصبیدن پودمان

   <div dir="ltr" align="left" markdown="1">

   ```shell
   # 1
   ./configure --sbin-path=/usr/sbin/nginx --conf-path=/etc/nginx/nginx.conf --error-log-path=/var/log/nginx/error.log \
            --http-log-path=/var/log/nginx/access.log --pid-path=/run/nginx.pid --with-pcre --with-http_ssl_module --with-http2_v2_module

   # 2 
   make

   # 3 
   make install 
   ```

   </div>

2. پیکریدن ssl: برای فعالیدن https به این نیازمند ایم.

   <div dir="ltr" align="left" markdown="1">

   ```shell
   mkdir /etc/nginx/ssl
   openssl req -x509 -days 10 -nodes -newkey rsa :2048 -keyout /etc/nginx/ssl/self.key -out /etc/nginx/ssl/self.crt
   ```

   </div>

3. فعالیدن ssl در nginx

   <div dir="ltr" align="left" markdown="1">

   ```conf
   # ...
   http {
      # ...
      server {

        listen 443 ssl;

        # ...

        ssl_certificate /etc/nginx/ssl/self.crt;
        ssl_certificate_key /etc/nginx/ssl/self.key

        # ...
      }
   }
   ```

   </div>

4. فعالیدن http2

   <div dir="ltr" align="left" markdown="1">

   ```conf
   # ...
   http {
      # ...
      server {

        listen 443 ssl http2;

        # ...
      }
   }
   ```

   </div>

**توجه.** برای استفیدن از http2 به پودمان ssh نیازمند ایم.

## هلیدن (push) چند منبع در یه درخواست http2

برای اینکه درخواست‌های کاربر به سمت سرور را بکاهیم، می‌توانیم منبع‌هایی که ممکن به‌زودی بدرخواستد را نیز با امریه ``http2_push`` در درخواستش بهلیم.

```conf
# ...
http {
  # ...
  server {
    # ...
    location = /index.html {
      # ...

      http2_push /style.css;
      http2_push /thumb.png;

      # ...
    }
    # ...
  }
}
```
