# امنیدن

## دگرمسیریدن http به https

بهتره همواره همه‌ی درخواست‌ها با پروتکل https باشد. برای این درخواست‌های http را هم به https می‌دگرمسیریم.

```conf
#...
http {
  #...

  server {
      listen 80;
      server_name 167.99.93.26;
      return 301 https://$host$request_uri;
  }

  server {
      listen 443 ssl http2;
      server_name 167.99.93.26;

      # ...

      # Enable HSTS
      add_header Strict-Transport-Security "max-age=31536000" always;
      
      # SSL sessions
      ssl_session_cache shared:SSL:40m;
      ssl_session_timeout 4h;
      ssl_session_tickets on;


      # ...
  }

  # ...
}
```

### جایگزیدن ssl با tls

```conf
#...
http {
  #...

  server {
      listen 443 ssl http2;
      server_name 167.99.93.26;
      
      # ...

      # Disable SSL
      ssl_protocols TLSv1 TLSv1.1 TLSv1.2;

      # Optimise cipher suite
      ssl_prefer_server_ciphers on;
      ssl_ciphers ECDH+AESGCM:ECDH+AES256:ECDH+AES128:DH+3DES:!ADH:!AECDH:!MD5;

      # Enable DH Params
      ssl_dhparam /etc/nginx/ssl/dhparam.pem;
      # ...
  }

  # ...
}
```

### فعالیدن پروتکل تبادل کلید دیفی-هلمن

پروتکل تبادل کلید دیفی-هلمن، یک پروتکل رمزنگاری است که با استفاده از آن، دو نفر یا دو سازمان، می‌توانند بدون نیاز به هر گونه آشنایی قبلی، یک کلید رمز مشترک ایجاد و آن را از طریق یک مسیر ارتباطی غیر امن، بین خود تبادل نمایند.

1. ساختن پارامتر dh:

   <div dir="ltr" align="left" markdown="1">

   ```shell
   openssl dhparam 2048 -out /etc/nginx/ssl/dhparam.pem
   ```

   </div>

2. فعالیدن پروتکل dh:

   <div dir="ltr" align="left" markdown="1">

    ```conf
    #...
    http {
    #...

    server {
        listen 443 ssl http2;
        server_name 167.99.93.26;
        
        # ...

        # Enable DH Params
        ssl_dhparam /etc/nginx/ssl/dhparam.pem;
        # ...
    }

    # ...
    }
    ```

   </div>

## محدودیدن آهنگ درخواست (Rate Limiting)

برپایه نشانی (ip) کاربر یا مسیر منبع می‌توان آهنگ درخواست را به شکل زیر محدودید.

```conf
#...
http {
  #...

  server {      
      # ...

      # Define limit zone
      limit_req_zone $binary_remote_address zone=addr:10m rate=60r/m; # 60r/m == 1r/s

      location / {
          # ...

          limit_req zone=addr burst=5;

          # ...
      }

      # ...
  }

  # ...
}
```

---
[منبع](https://docs.nginx.com/nginx/admin-guide/security-controls/controlling-access-proxied-http/)

## احرازیدن هویت پایه (Basic Auth)

برای محدودیدن دسترسی به بخشی از سایت:

1. نصبیدن `apache2-utils`

   <div dir="ltr" align="left" markdown="1">

   ```shell
   sudo apt install apache2-utils
   ```

   </div>

2. ثبتیدن کاربر

   <div dir="ltr" align="left" markdown="1">

   ```shell
   htpasswd -c /etc/nginx/.htpasswd username
   ```

   </div>

3. پیکریدن انجین‌ایکس

   <div dir="ltr" align="left" markdown="1">

   ```conf
    #...
    http {
        #...

        server {      
            # ...

            location / {
                # ...

                
                auth_basic "Secure Area"; # Message
                auth_basic_user_file /etc/nginx/.htpasswd; # password file

                # ...
            }          

            # ...
        }

    # ...
    }
    ```

   </div>

## پنهانیدن نسخه‌ی انجین‌ایکس

```conf
#...
http {
  #...

  server_tokens off;

  # ...
}
```

## جلوگیریدن از نمودن سایتمان در یه سایت دیگر

```conf
#...
http {
  #...

  server {
      # ...

      add_header X-Frame-Options "SAMEORIGIN";
      add_header X-XSS-Protection "1; mode=block";

      # ...
  }

  # ...
}
```

## حذفیدن پودمان‌های غیرضروری انجین‌ایکس

بازنصبیدن انجین‌ایکس بدون پودمان‌های غیرضروری:

```shell
# To able to compile the source code
sudo apt-get install -y build-essential
# Dependencies
sudo apt-get install libpcre3 libpcre3-dev zlib1g zlib1g-dev libssl-dev

# To find latest version: http://nginx.org/en/download.html
wget http://nginx.org/download/nginx-VERSION.tar.gz -P /tmp/
tar -zxvf nginx-VERSION.tar.gz

# Enter to where the nginx file extracted
cd nginx-VERSION

# All available options: http://nginx.org/en/docs/configure.html
./configure --sbin-path=/usr/sbin/nginx --conf-path=/etc/nginx/nginx.conf --error-log-path=/var/log/nginx/error.log \
    --http-log-path=/var/log/nginx/access.log --pid-path=/run/nginx.pid --with-pcre --with-http_ssl_module --without-http_autoindex_module
# To compile this configuration code
sudo make
# To install the compiled source
sudo make install

# To check nginx available
nginx -V

# To start nginx
sudo nginx

# To check the process are running
ps aux | grep nginx
```
