# پیکریدن

برای پیکریدن انجین‌ایکس کافیه فرمان‌ها در فایل پیکربند نوشته شوند.

**مثال ساختن یه میزبان مجازی پایه‌ای.**

نخست  میزبان‌ها در فایل پیکربند `/etc/nginx/nginx.conf` تعریفیده می‌شوند:

<div dir="ltr" align="left" markdown="1">

```conf
events {}

http {
  # To include mime.types;
  include mime.types;

  # تعینیدن نوع یه فایل براساس پسوندش
  types {
    text/css css;
  }

  # تعریفیدن یه میزبان مجازی
  server {
    # گوشیدن به درگاه ۸۰ پروتکل http
    listen 80;
    # To define the server name witch is a domain, subdomain, or IP address
    # Ex. *.mydomain.com
    server_name 192.168.42.170;
    # To define the root directory
    root /home/amookhsin/app;
  }
}
```

</div>

در انتها برای اعمال تغییرها انجین‌ایکس باید بازبارگذایده شود:

<div dir="ltr" align="left" markdown="1">

```shell
# To test the nginx file configuration
sudo nginx -t

sudo systemctl reload nginx
```

</div>

**توجه.** اگر nginx را از روی منبع نصبیده شده باشد، مسیر فایل پیکربند `--conf-path=/etc/nginx/nginx.conf` توسط این پارامتر تعیینیده شده.

## مفهوم‌های پایه

### انواع `directive` و اثرشان بر ارثبری

ارثبریدن هر زیرکانتکس از پیکره‌های زبرکانتکس‌اش وابسته به نوع امریه‌ها است که شامل سه نوع هستند:

<div dir="rtl" align="right" markdown="1">

* امریه‌های استاندارد (standard directive): تنها یک مقدار دارد که توسط آخرین اعلام مشخص می‌شود؛ مانند، `root`.
* امریه‌های آرایه‌ای (array directive): هر کانتکس می‌تواند برای خودش داشته‌باشد بدون اثر پایین‌به‌بالا (اثر بر تنظیم زبرکانتکس)؛ مانند، `access_log`.
* امریه‌‌های فعالیتی (action directive): امریه‌هایی که برای انجامیدن یه فعالیت هستند که ارثبرده نمی‌شوند و تنها باعث پایاندن (مانند، `return`) یا بازارزیابیدن (مانند، `rewrite`) می‌شوند.

</div>

```conf
events {}

http {
  # ...

  server {
    # ...
    root /home/amookhsin/app;
    # ...
  }

  location /greet {
    # inherited root
    # root /home/amookhsin/app;
  }
}
```

### متغیرها

[متغیرهای درونی nginx](http://nginx.org/en/docs/varindex.html)

نحو تعریفیدن متغیر `set $var_name 'val';`.

**نکته.** نوع-داده‌های مجاز تنها رشته، عدد صحیح و ارزش منطقی است.

```conf
location /only-one-if {
    set $true 1;

    if ($true) {
        add_header X-First 1;
    }

    return 204;
}
```

### عبارت‌های شرطی (conditional statement)

[مرجع](https://www.nginx.com/resources/wiki/start/topics/depth/ifisevil/)

نحو عبارت شرطی `if`:

```conf
if ($request_method = POST ) {
  return 405;
}
if ($args ~ post=140){
  rewrite ^ http://example.com/ permanent;
}
```

## مسیریدن درخواست به منبع

با استفیدن از امریه `location` می‌توان درخواست از یه URI را به یه منبع خاص مسیرید. که نحوش همانند زیر است:

```conf
location [modifier] [URI] {
  ...
  ...
}
```

![امریه location](assets/location_directive.png)

**مثال.**

```conf
events {}

http {
  include mime.types;

  types {
    text/css css;
  }

  server {
    listen 80;
    server_name 192.168.42.170;
    root /home/amookhsin/app;
  }

  # To define a special behavior for /greet*
  location /greet {
    return 200 'Hi there!'
  }
}
```

### اصلاحنده (modifier)

انواع اصلاحنده‌ها به ترتیب اولویت‌اشان:

1. دقیق (exact): `location = [URI] {}`؛
2. پیوندی بااولویت: `location ^~ [URI] {}`؛
3. عبارت باقاعده (regEx): حساس `location ~ [URI] {}` و ناحساس `location ~* URI {}`;
4. پیشوندی (prefix): `location [URI] {}`

**نکته.** خط‌های بالایی نیز دارای اولویت بیشتری هستند.

## بازمسیریدن (redirecting)

با استفیدن امریه‌های (directive) زیر می‌توان یه درخواست را بازمسیرید:

<div dir="rtl" align="right" markdown="1">

1. `return [status] [URI];`: برگرداندن مسیر جدید.

   <div dir="ltr" align="left" markdown="1">

   ```conf
   location /logo {
     return 307 /thumb.png;
   }
   ```

   </div>

2. `rewrite [pattern] [URI];`: دگریدن URI و بازارزیدن توسط nginx به‌عنوان یه درخواست جدید. برای همین نسب به `return` منبع‌های بیشتری را می‌استفد.

   <div dir="ltr" align="left" markdown="1">

   ```conf
   rewrite ^/user/\w+ /greet;

   local /greet {
     return 200 "Hello User";
   }
   ```

   </div>

3. `try_files path_1 path_2 final;`: دگریدن URI درصورت یافت‌نشدن منبع‌های مشخص‌شده در ریشه بدون درنظر گرفتن URI درخواست.

   <div dir="ltr" align="left" markdown="1">

   ```conf
   http {
     server {
       # ...
       try_files $uri /friendly_404;

       location /friendly_404 {
         return 404 "Sorry, that file could not be found.";
       }
       # ...
     }
   }
   ```

   </div>

</div>

## نامیدن یه `location`

با فرانویسه‌ی `@` می‌توان یه `location` را نامید و بهش ارجاعید:

```conf
http {
  server {
    # ...
    try_files $uri @friendly_404;

    location @friendly_404 {
      return 404 "Sorry, that file could not be found.";
    }
    # ...
  }
}
```

## روینگاشتن

پیشفرض روینگاشت‌های خطا و دسترسی در مسیر `/var/log/nginx` انبار می‌شوند که با امریه‌های `access_log` و `error_log` می‌توان به‌ازای هر URI مسیر دیگری را تعیینید.

```conf
http {
  server {
    # ...
    location /secure {
      access_log /var/log/nginx/secure.access.log;
      return 200 "Welcome to secure area.";
    }
    # ...
  }
}
```

**توجه.** برای نافعالیدن روینگاشت کافیه بجای تعریف مسیر از `off` باستفیم: `access_log off;`.

[مرجع روینگاشتن](https://docs.nginx.com/nginx/admin-guide/monitoring/logging/)

## تعیینیدن کاربر

تعیینیدن کاربر اجراینده انجین‌ایکس

```conf
# To define user
user www-data;

events {
  # ...
}

http {
  # ...
}
```

## پیکریدن پردازشگرهای انجین‌ایکس

از امریه `worker_processes` برای افزودن شمار پردازشگرهای شنونده انجین‌ایکس می‌استفیم.

**توجه.** تعداد پردازشگرهای پیشفرض یکی است.

```shel
sudo systemctl status nginx
# OR
sudo ps aux | grep nginx
```

**مثال.**

```conf
worker_processes 2;
# To auto it depends on CPU's core
#worker_processes auto;
```

**توجه.** بهتره شمار پردازشگرهای انجین‌ایکس برابر با هسته‌های پردازنده باشد.

```shell
# To get the number of CPU's cores
nproc
# OR
lscpu
```

![انجین‌ایکس-بهینه‌ترین تعداد پردازشگر](assets/nginx_processes.png)

## پیکریدن شمار اتصال‌ها

برای تعیینیدن شمار بیشینه استفنده‌های متصلیده به هر پردازشگر سرور به‌شکل همزمان از امریه `worker_connections` می‌استفیم که در کانتکس `events` تعریفیده می‌شود.

```conf
events {
  worker_connections 1024;
}
```

**توجه.** شمار اتصال‌ها محدود به بیشینه تعداد فایل‌های باز سرور است.

```shell
# To get the number of open file descriptors for each process
ulimit -n
```

**نکته.** تعداد استفنده‌های متصلیده به سرور به‌شکل همزمان از ضرب `worker_processes` در `worker_connections` بدست می‌آید.

## واژه‌شناسی

دو عبارت اصلی که برای فهمیدن مستندهای nginx نیز مهم است:

<div dir="rtl" align="right" markdown="1">

* directive: گزینه‌های مشخص در فایل پیکره که شامل کلید و مقدار است؛ مانند: `server_name my_domain.com;`.
* context: یه بخش در فایل پیکره که directiveهایی درونش نهاده‌شده اند. مهمترین‌اشان:
  * `http`: مرتبط به هرچیز مربوط به http.
  * `server`: میزبان‌های مجازی
  * `location`

</div>

<div dir="rtl" align="right" markdown="1">

## Buffer and Timeouts

</div>

برای بهینیدن سایتمان.

```conf
user www-data;

worker_processes auto;

events {
  worker_connections 1024;
}

http {

  include mime.types;

  # Buffer size for POST submissions
  client_body_buffer_size 10K;
  client_max_body_size 8m;

  # Buffer size for Headers
  client_header_buffer_size 1k;

  # Max time to receive client headers/body
  client_body_timeout 12;
  client_header_timeout 12;

  # Max time to keep a connection open for
  keepalive_timeout 15;

  # Max time for the client accept/receive a response
  send_timeout 10;

  # Skip buffering for static files
  sendfile on;

  # Optimise sendfile packets
  tcp_nopush on;

  server {

    listen 80;
    server_name 167.99.93.26;

    root /sites/demo;

    index index.php index.html;

    location / {
      try_files $uri $uri/ =404;
    }

    location ~\.php$ {
      # Pass php requests to the php-fpm service (fastcgi)
      include fastcgi.conf;
      fastcgi_pass unix:/run/php/php7.1-fpm.sock;
    }

  }
}
```

## استفیدن از پودمان

با استفیدن از پودمان‌ها می‌توان عملکرد انجین‌ایکس را گسترد.

```conf
user www-data;

worker_processes auto;

load_module modules/ngx_http_image_filter_module.so;

events {
  worker_connections 1024;
}

http {

  include mime.types;

  # Buffer size for POST submissions
  client_body_buffer_size 10K;
  client_max_body_size 8m;

  # Buffer size for Headers
  client_header_buffer_size 1k;

  # Max time to receive client headers/body
  client_body_timeout 12;
  client_header_timeout 12;

  # Max time to keep a connection open for
  keepalive_timeout 15;

  # Max time for the client accept/receive a response
  send_timeout 10;

  # Skip buffering for static files
  sendfile on;

  # Optimise sendfile packets
  tcp_nopush on;

  server {

    listen 80;
    server_name 167.99.93.26;

    root /sites/demo;

    index index.php index.html;

    location / {
      try_files $uri $uri/ =404;
    }

    location ~\.php$ {
      # Pass php requests to the php-fpm service (fastcgi)
      include fastcgi.conf;
      fastcgi_pass unix:/run/php/php7.1-fpm.sock;
    }

    location = /thumb.png {
      image_filter rotate 180;
    }

  }
}
```
